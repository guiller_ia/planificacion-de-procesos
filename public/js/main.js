
/* global procesos */

$(document).ready(function () {
    nombres = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
    count = 0;
    procesos = [];

    $("#addButton").click(addProceso);
    $("#delButton").click(delProceso);
    $("#fcfsButton").click(calcFCFS);
    $("#sjnButton").click(calcSJN);
    $("#rrButton").click(calcRR);

    function calcRR() {
        loadProcesos();
        quantum = Number($("#quantum").val());
        console.assert(procesos.length > 0, "No hay procesos!!");
        procesos.sort(ordenarPr);
        acumulador = 0;
        secuencia="";
        while (!estaCompleto()) {
            for (var i = 0; i < procesos.length; i++) {
                if (procesos[i].HL <= acumulador && !procesos[i].Hecho) {
                    acumulador += quantum;
                    procesos[i].Progreso += quantum;
                    secuencia+=procesos[i].Pr;
                    if (procesos[i].Progreso >= procesos[i].TC) {
                        procesos[i].Hecho = true;
                        acumulador -= (procesos[i].Progreso - procesos[i].TC);
                        procesos[i].HS = acumulador;
                        fillGaps(procesos[i]);
                    }
                }
            }
            secuencia+="-";
        }
        showResultados();
        $("#secuencia").html("Secuencia: "+secuencia);
    }
    
    function calcSJN() {
        loadProcesos();
        console.assert(procesos.length > 0, "No hay procesos!!");
        procesos.sort(ordenarHLTC);
        acumulador = procesos[0].HL;
        while (!estaCompleto()) {
            cola = subConjunto(acumulador);
            if (cola.length === 0) {
                acumulador=findMinHL();
                continue
            }
            cola.sort(ordenarTC);
            if (cola.length > 0) {
                acumulador += cola[0].TC;
                cola[0].HS = acumulador;
                cola[0].Hecho = true;
                fillGaps(cola[0]);
            }
        }
        showResultados();
    }
    
    function findMinHL(){
        primero=true;
        min=0;
        for (var i = 0; i < procesos.length; i++) {
            if(!procesos[i].Hecho && primero){
                min=procesos[i].HL;
                primero=false;
            }
            if(!primero && !procesos[i].Hecho && procesos[i].HL<min){
                min=procesos[i].HL;
            }
        }
        return min;
    }
    
    function subConjunto(ac){
        ans=[];
        for (var i = 0; i < procesos.length; i++) {
            if (procesos[i].HL <= ac && !procesos[i].Hecho) {
                ans.push(procesos[i]);
            }
        }
        return ans;
    }

    function estaCompleto() {
        for (var i = 0; i < procesos.length; i++) {
            if (!procesos[i].Hecho)
                return false;
        }
        return true;
    }

    function calcFCFS() {
        loadProcesos();
        procesos.sort(ordenarHL);
        acumulador = 0;
        for (var i = 0; i < procesos.length; i++) {
            if (procesos[i].HL > acumulador) {
                acumulador = procesos[i].HL;
            }
            acumulador += procesos[i].TC;
            procesos[i].HS = acumulador;
            procesos[i].Hecho = true;
            fillGaps(procesos[i]);
        }
        showResultados();
    }

    function showResultados() {
        procesos.sort(ordenarPr);
        showProcesos();
        showPromedios();
        $("#secuencia").html("");
    }

    function showPromedios() {
        promTS = 0;
        promTE = 0;
        promIS = 0;
        for (var i = 0; i < procesos.length; i++) {
            promTS += procesos[i].TS;
            promTE += procesos[i].TE;
            promIS += Number(procesos[i].IS);
        }
        promTS /= count;
        promTE /= count;
        promIS /= count;
        promTS = formatFloat(promTS);
        promTE = formatFloat(promTE);
        promIS = formatFloat(promIS);
        $("#promedios").html(
                "promedio TS: " + promTS + "<br>" +
                "promedio TE: " + promTE + "<br>" +
                "promedio IS: " + promIS + "<br>&#x2593;");
    }

    function formatFloat(fnum) {
        return (parseFloat(Math.round(fnum * 1000) / 1000).toFixed(3));
    }

    function ordenarPr(a, b) {
        if (a.Pr < b.Pr)
            return -1;
        if (a.Pr > b.Pr)
            return 1;
        return 0;
    }

    function ordenarHLTC(a, b) {
        if (a.HL < b.HL)
            return -1;
        if (a.HL > b.HL)
            return 1;
        if (a.TC < b.TC)
            return -1;
        if (a.TC > b.TC)
            return 1;
        return 0;
    }

    function ordenarHL(a, b) {
        return(a.HL - b.HL);
    }
    
    function ordenarTC(a, b){
        return(a.TC - b.TC);
    }

    function fillGaps(proc) {
        proc.TS = proc.HS - proc.HL;
        proc.TE = proc.TS - proc.TC;
        proc.IS = formatFloat(proc.TC / proc.TS);
    }

    function showProcesos() {
        $("#genTBody").html("");
        for (var i = 0; i < procesos.length; i++) {
            $("#genTBody").append("<tr><td>" + procesos[i].Pr +
                    "</td><td>" + procesos[i].HL +
                    "</td><td>" + procesos[i].TC +
                    "</td><td>" + procesos[i].HS +
                    "</td><td>" + procesos[i].TS +
                    "</td><td>" + procesos[i].TE +
                    "</td><td>" + procesos[i].IS +
                    "</td></tr>");
        }
    }

    function loadProcesos() {
        //cargar los procesos desde la tabla de entrada
        procesos = [];
        var Proc = makeStruct("Pr HL TC HS TS TE IS Hecho Progreso");
        $("#inputTable").children("tr").each(function () {
            pr = $(this).text();
            hl = Number($(this).find("input").val());
            tc = Number($(this).find("td:last-child").find("input").val());
            var proceso = new Proc(pr, hl, tc, 0, 0, 0, 0, false, 0);
            procesos.push(proceso);
        });
    }

    function makeStruct(names) {
        var names = names.split(' ');
        var counter = names.length;
        function process() {
            for (var i = 0; i < counter; i++) {
                this[names[i]] = arguments[i];
            }
        }
        return process;
    }

    function delProceso() {
        if (count > 0) {
            $("#inputTable").find('tr:last-child').remove();
            count--;
        }
    }

    function addProceso() {
        nombre = nombres[count];
        count++;
        llegada = Math.floor((Math.random() * 20));
        ejecucion = Math.floor((Math.random() * 20) + 1);
        $("#inputTable").append("<tr><td>" + nombre + "</td>" +
                '<td><input type="number" name="llegada" min=0 max=99 value="' + llegada + '"></td>'
                + '<td><input type="number" name="ejecucion" min=0 max=99 value="' + ejecucion + '"></td></tr>');
    }
});
